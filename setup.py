#  _____     _                       
# |_   _| __(_)_ __   ___  _ __ __ _ 
#   | || '__| | '_ \ / _ \| '__/ _` |
#   | || |  | | | | | (_) | | | (_| |
#   |_||_|  |_|_| |_|\___/|_|  \__,_|
#                                    
# This program is licensed under the terms detailed in the LICENSE.md file.

"""
Distutils package setup script.
"""

__author__ = "Arthur Lucas"		# FIXME
__email__ = "alucas1@trinora.com"		# FIXME
__copyright__ = "Copyright 2013 Trinora, Inc. All rights reserved."

import os
from setuptools import setup, find_packages

def read(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).read()
	
def read_firstline(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).readline()

setup(
	name = "django-blank-app",		# FIXME
	version = "0.1",		# FIXME
	url = "http://repo.trinora.com/django-blank-app",		# FIXME
	license = read_firstline("LICENSE.md"),
	description = "A skeleton setup for a Django application.",		# FIXME
	long_description = read("README.md"),
	
	author = "Arthur Lucas",		# FIXME
	author_email = "alucas1@trinora.com",		# FIXME
	
	packages = find_packages("src"),
	package_dir = {"": "src"},
	
	install_requires = [		# FIXME
		"setuptools",
		"django",
		"django-appconf",
		"automodinit"
	],
	
	classifiers = [		# FIXME
		"Development Status :: 4 - Beta",
		"Framework :: Django",
		"Intended Audience :: Developers",
		"Operating System :: OS Independent",
		"Programming Language :: Python",
		"Topic :: Internet :: WWW/HTTP",
	]
)