#  _____     _                       
# |_   _| __(_)_ __   ___  _ __ __ _ 
#   | || '__| | '_ \ / _ \| '__/ _` |
#   | || |  | | | | | (_) | | | (_| |
#   |_||_|  |_|_| |_|\___/|_|  \__,_|
#                                    
# This program is licensed under the terms detailed in the LICENSE.md file.

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

__author__ = "Arthur Lucas"		# FIXME
__email__ = "alucas1@trinora.com"		# FIXME
__copyright__ = "Copyright 2013 Trinora, Inc. All rights reserved."

from django.test import TestCase

class SimpleTest(TestCase):
	def test_basic_addition(self):
		"""
		Tests that 1 + 1 always equals 2.
		"""
		self.assertEqual(1 + 1, 2)
